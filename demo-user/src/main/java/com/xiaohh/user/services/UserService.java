package com.xiaohh.user.services;

import com.xiaohh.user.entities.User;

import java.util.List;

/**
 * <p>
 * user 对象的业务处理层
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-02-22 星期一 20:07:12
 * @file UserService.java
 */
public interface UserService {

    /**
     * 查询所有的用户对象并返回
     * @return 用户列表
     */
    List<User> list();

    /**
     * 添加一个用户
     * @param user 用户对象
     * @return 是否成功
     */
    Boolean add(User user);

    /**
     * 根据 id 获取用户对象
     * @param id 用户 id
     * @return 用户对象
     */
    User get(long id);

    /**
     * 更新用户对象，用户对象的 id 不能为空
     * @param user 用户对象
     * @return 是否成功
     */
    boolean update(User user);

    /**
     * 根据用户 id 删除用户
     * @param id 用户 id
     * @return 是否成功
     */
    boolean delete(long id);
}
