package com.xiaohh.user.constants;

/**
 * <p>
 * 异常统一声明
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-02-22 星期一 20:03:50
 * @file BizCodeEnum.java
 */
public enum BizCodeEnum {

    /**
     * 未知异常
     */
    UNKNOWN_EXCEPTION(10000, "系统未知异常"),

    /**
     * 参数校验异常
     */
    VALIDATE_EXCEPTION(10001, "参数格式校验失败"),

    /**
     * 数据完整性异常
     */
    DATA_INTEGRITY_VIOLATION_EXCEPTION(10002, "数据完整性校验失败"),

    /**
     * SQL 语句错误
     */
    BAD_SQL_GRAMMAR_EXCEPTION(10003, "SQL 语句错误");

    /**
     * 错误码
     */
    private int code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * @param code    错误码
     * @param message 错误信息
     */
    BizCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 获取错误码
     *
     * @return 错误码
     */
    public int getCode() {
        return code;
    }

    /**
     * 获取错误信息
     *
     * @return 错误信息
     */
    public String getMessage() {
        return message;
    }
}
