package com.xiaohh.user.mappers;

import com.xiaohh.user.entities.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * user 对象的业务访问层
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-02-22 星期一 20:10:27
 * @file UserMapper.java
 */
@Mapper
public interface UserMapper {

    /**
     * 向数据库中插入一个用户数据
     *
     * @param user 用户数据的对象
     * @return 受影响的行数
     */
    int insert(User user);

    /**
     * 查询所有的用户对象并返回
     *
     * @return 用户列表
     */
    List<User> list();

    /**
     * 根据 id 获取用户对象
     *
     * @param id 用户 id
     * @return 用户对象
     */
    User get(@Param("id") long id);

    /**
     * 更新用户对象，用户对象的 id 不能为空
     *
     * @param user 用户对象
     * @return 受影响行数
     */
    int update(User user);

    /**
     * 根据用户 id 删除用户
     *
     * @param id 用户 id
     * @return 受影响行数
     */
    int delete(@Param("id") long id);
}
