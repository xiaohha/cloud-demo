package com.xiaohh.user.ws;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 * 用户的 WebSocket
 * </p>
 *
 * @author tanghai
 * @version 1.0
 * @date 2021-08-17 星期二 14:24:11
 */
@Slf4j
@Component
@ServerEndpoint("/ws/user")
public class UserWebSocket {

    /**
     * 用于记录当前在线人数
     */
    private static final AtomicInteger onlineCount = new AtomicInteger(0);

    /**
     * Session 列表
     */
    private static final Map<String, Session> sessionMap = new HashMap<String, Session>();

    /**
     * 打开一个websocket
     *
     * @param session WebSocket 的 session 对象
     */
    @OnOpen
    public void onOpen(Session session) {
        final int i = onlineCount.incrementAndGet();
        final String id = session.getId();
        sessionMap.put(id, session);
        log.info("有新的链接加入：{}，当前在线人数为：{}。", id, i);
    }

    /**
     * 关闭一个链接
     *
     * @param session 关闭链接的 session
     */
    @OnClose
    public void onClose(Session session) {
        final int i = onlineCount.decrementAndGet();
        sessionMap.remove(session.getId());
        log.info("有新的链接关闭：{}，当前在线人数为：{}。", session.getId(), i);
    }

    /**
     * 发生错误
     *
     * @param session 错误发生的会话对象
     * @param error   错误对象
     */
    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 服务端收到客户端发过来的消息
     *
     * @param message 消息内容
     * @param session 会话对象
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        log.info("服务端收到客户端[{}]的消息:{}", session.getId(), message);
        this.sendMessage("Hello, " + message, session);
    }

    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, Session toSession) {
        log.info("服务端给客户端[{}]发送消息{}", toSession.getId(), message);
        sessionMap.values().forEach(session -> {
            try {
                session.getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
