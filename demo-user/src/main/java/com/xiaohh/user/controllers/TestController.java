package com.xiaohh.user.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 测试使用的 controller
 * </p>
 *
 * @author XiaoHH
 * @version 1.0
 * @date 2021-02-22 星期一 19:05:19
 * @file TestController.java
 */
@RestController
@RequestMapping("/v1/test")
public class TestController {

    @GetMapping
    public String hello() {
        return "hello";
    }
}
